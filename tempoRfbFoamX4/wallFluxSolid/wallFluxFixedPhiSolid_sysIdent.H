/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::wallFluxFixedPhiSolid_sysIdentFvPatchScalarField

Description
    Hydrostatic pressure boundary condition calculated as


SourceFiles
    wallFluxFixedPhiSolid_sysIdentFvPatchScalarField.C

\*---------------------------------------------------------------------------*/

#ifndef wallFluxFixedPhiSolid_sysIdentFvPatchScalarField_H
#define wallFluxFixedPhiSolid_sysIdentFvPatchScalarField_H

#include "fixedValueFvPatchFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
          Class wallFluxFixedPhiSolid_sysIdentFvPatch Declaration
\*---------------------------------------------------------------------------*/

class wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
:
    public fixedValueFvPatchScalarField
{
    // Private data



public:

    //- Runtime type information
    TypeName("wallFluxFixedPhiSolid_sysIdent");


    // Constructors

        //- Construct from patch and internal field
        wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given
        //  wallFluxFixedPhiSolid_sysIdentFvPatchScalarField onto a new patch
        wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
        (
            const wallFluxFixedPhiSolid_sysIdentFvPatchScalarField&,
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
        (
            const wallFluxFixedPhiSolid_sysIdentFvPatchScalarField&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchScalarField> clone() const
        {
            return tmp<fvPatchScalarField>
            (
                new wallFluxFixedPhiSolid_sysIdentFvPatchScalarField(*this)
            );
        }

        //- Construct as copy setting internal field reference
        wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
        (
            const wallFluxFixedPhiSolid_sysIdentFvPatchScalarField&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchScalarField> clone
        (
            const DimensionedField<scalar, volMesh>& iF
        ) const
        {
            return tmp<fvPatchScalarField>
            (
                new wallFluxFixedPhiSolid_sysIdentFvPatchScalarField
                (
                    *this,
                    iF
                )
            );
        }


    // Member functions

        // Access


 /*           //- Return the reference pressure
            scalar phiSolidRef() const
            {
                return phiSolidRef_;
            }

            //- Return reference to the reference pressure to allow adjustment
            scalar& phiSolidRef()
            {
                return phiSolidRef_;
            }

            //- Return the reference pressure
            scalar extResistance() const
            {
                return extResistance_;
            }

            //- Return reference to the reference pressure to allow adjustment
            scalar& extResistance()
            {
                return extResistance_;
            }

*/
        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();


        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
