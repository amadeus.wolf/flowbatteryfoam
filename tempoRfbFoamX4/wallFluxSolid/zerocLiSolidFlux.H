/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::zerocLiSolidFluxFvPatchScalarField

Description
    Hydrostatic pressure boundary condition calculated as


SourceFiles
    zerocLiSolidFluxFvPatchScalarField.C

\*---------------------------------------------------------------------------*/

#ifndef zerocLiSolidFluxFvPatchScalarField_H
#define zerocLiSolidFluxFvPatchScalarField_H

#include "fixedValueFvPatchFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
          Class zerocLiSolidFluxFvPatch Declaration
\*---------------------------------------------------------------------------*/

class zerocLiSolidFluxFvPatchScalarField
:
    public fixedValueFvPatchScalarField
{
    // Private data




public:

    //- Runtime type information
    TypeName("zerocLiSolidFlux");


    // Constructors

        //- Construct from patch and internal field
        zerocLiSolidFluxFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        zerocLiSolidFluxFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given
        //  zerocLiSolidFluxFvPatchScalarField onto a new patch
        zerocLiSolidFluxFvPatchScalarField
        (
            const zerocLiSolidFluxFvPatchScalarField&,
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        zerocLiSolidFluxFvPatchScalarField
        (
            const zerocLiSolidFluxFvPatchScalarField&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchScalarField> clone() const
        {
            return tmp<fvPatchScalarField>
            (
                new zerocLiSolidFluxFvPatchScalarField(*this)
            );
        }

        //- Construct as copy setting internal field reference
        zerocLiSolidFluxFvPatchScalarField
        (
            const zerocLiSolidFluxFvPatchScalarField&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchScalarField> clone
        (
            const DimensionedField<scalar, volMesh>& iF
        ) const
        {
            return tmp<fvPatchScalarField>
            (
                new zerocLiSolidFluxFvPatchScalarField
                (
                    *this,
                    iF
                )
            );
        }


    // Member functions

        // Access



        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();


        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
