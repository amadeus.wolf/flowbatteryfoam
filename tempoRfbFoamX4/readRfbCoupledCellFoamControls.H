    // We do not have a top-level mesh. Construct the fvSolution for
    // the runTime instead.
    fvSolution solutionDict(runTime);

    const dictionary& cellControls = solutionDict.subDict("CellControls");


// at beginning of simulation nOuterCorrectors has to be increased
    static int nOuterCorr;

    scalar stepSize = 1e-6;

    if (mag(runTime.startTime().value() + runTime.deltaT().value() - runTime.value() ) < SMALL )
    nOuterCorr = 2;	//8;
    else
    nOuterCorr = cellControls.lookupOrDefault<int>("nOuterCorrectors", 1);

    
// flag is true when iE is no more conservative --> End of calculation
//    endCalc = runTime.controlDict().lookupOrDefault("endCalc", false);


// obtaining outputTimes from controlDict

    const scalar conservativeTolerance =
        cellControls.lookupOrDefault<scalar>("conservativeTolerance", 5e-7);







