
// at beginning of simulation nCorr has to be increased
int nCorr = mincPhiIterationsPtr[i];

label patchID_cc = solidRegions[i].boundaryMesh().findPatchID("cathode_currentcollector");
label patchID_cte = solidRegions[i].boundaryMesh().findPatchID("cathode_to_electrolyte");

//dimensionedScalar phiSolid_cc("phiSolid_cc", dimensionSet(1,2,-3,0,0,-1,0), scalar(0.0));
//dimensionedScalar phiSolidNew("phiSolidNew", dimensionSet(1,2,-3,0,0,-1,0), scalar(0.0));

scalar phiSolid_cc = 0.0;
scalar phiSolidNew;

//scalar Itot = gSum(								// gSum = Sum over all processors in parallel run
//  		-conductSolidPtr[i].boundaryField()[patchID_cte]		// electrical conductivity at all faces of cte patch
//  		*(phiSolidPtr[i].boundaryField()[patchID_cte].snGrad()		// snGrad() calculates the surface normal gradient 
//  		* solidRegions[i].magSf().boundaryField()[patchID_cte]));	// magSf() calculates face area  


scalar Itot = gSum(-conductSolidPtr[i].boundaryField()[patchID_cte]*(phiSolidPtr[i].boundaryField()[patchID_cte].snGrad() * solidRegions[i].magSf().boundaryField()[patchID_cte]));

// why is Itot not totalCurrentPtr[i] ?
// scalar Itot = totalCurrentPtr[i];


Info << nl << "Itot at cte is " << Itot << endl;



