bool firstSolidConvergence = false;

	// first convergence criteria: 
	//scalar checkInternalCurrent = mag(I_cte-I_cc)/mag(I_cte);
	scalar checkInternalCurrent = mag(mag(I_cc)-mag(I_cte))/mag(I_cc);
	scalar epsilonSolid1 = 0.001;

	//Info << "|I_cc - I_cte| / |I_cc| < epsilonSolid1 : " << checkInternalCurrent << " < " << epsilonSolid1 << nl << endl;
	Info << "(|I_cc| - |I_cte|) / |I_cc| < epsilonSolid1 : " << checkInternalCurrent << " < " << epsilonSolid1 << nl << endl;

	// second convergence criteria (accounts for galvanostatic calculation)
	//scalar checkSolidTotalCurrent = (mag(totalCurrentPtr[i]-I_cte)/max(mag(totalCurrentPtr[i]),VSMALL));
	scalar checkSolidTotalCurrent = (mag(totalCurrentPtr[i]-I_cte)/mag(totalCurrentPtr[i]));
	scalar epsilonSolid2 = 0.1;
	
	Info << "|I_set - I_cte| / |I_set| < epsilonSolid2 : " << checkSolidTotalCurrent << " < " << epsilonSolid2 << nl << endl;

	// Check phiSolid convergence
	iPhiSol++;

	if (iPhiSol + 1 == nCorr)
        {
                nCorr = nCorr + 5;
                phiSolidConvergence = false;
        }


        if (nCorr > 500)
	{
                phiSolidConvergence = true;		
		Info << "phiSolidConvergence = true by reaching max. iterations !" << nl << endl;
     	}

	else if (checkInternalCurrent < epsilonSolid1)
        {
                firstSolidConvergence = true;
		Info << "|I_cc - I_cte| / |I_cc| < epsilonSolid1 is true !" << nl << endl;
        }
	
	// potentiosatatic calculation
	if(firstSolidConvergence == true)
	{
		phiSolidConvergence = true;
	}

	
	// Galavanostatic calculation -> change phiSolid_cc to account for constant current
/*	if (firstSolidConvergence == true)
	{
		if(checkSolidTotalCurrent < epsilonSolid2)
		{
			phiSolidConvergence = true;
			Info << "|I_set - I_cte| / |I_set| < epsilonSolid2 is true ! " << nl << endl;
		}	
		else
		{	// change phiSolid_cc 
			forAll (phiSolidPtr[i].boundaryField()[patchID_cc],facei)
		        {
                		phiSolidPtr[i].boundaryFieldRef()[patchID_cc][facei] -= 0.1 ;
        		}
			Info << "Changing phiSolid_cc !" << nl << endl;
		}	
	}

*/
