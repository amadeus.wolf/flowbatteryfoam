// entries can be found as subDict in system/fluid/fvSolution
const dictionary& potControl = mesh.solutionDict().subDict("FluidPotentialCalculation");

//const int nCorr = potControl.lookupOrDefault<int>("nCorrectors", 1);

scalar nfCorr(readScalar(potControl.lookup("nCorrectors")));
scalar nfCorrMax(readScalar(potControl.lookup("nCorrectorsMaximum")));

scalar tolMemEtc(readScalar(potControl.lookup("tolMemEtc")));
scalar tolEtcTotal(readScalar(potControl.lookup("tolEtcTotal")));

scalar convergenceTolerance(readScalar(potControl.lookup("convergenceTol")));

scalar phiResidual = readScalar(potControl.lookup("initPotResidual"));

scalar fluidCorrMin = readScalar(potControl.lookup("fluidCorrMin"));

Info << "Reading electrolyte potential calculation parameters." << endl;
Info << "nCorrectors = " << nfCorr << token::TAB << "convergenceTol = " << convergenceTolerance << token::TAB << "initPotResidual = " << phiResidual << nl << endl;
