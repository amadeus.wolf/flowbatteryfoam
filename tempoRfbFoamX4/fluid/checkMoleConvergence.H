// checking mole convergence 
         
	scalar flux_in = -gSum(mag(UFluidPtr[i].boundaryField()[patchID_in])*fluidRegions[i].magSf().boundaryField()[patchID_in]*
			 (cTempoElecPtr[i].boundaryField()[patchID_in]+cTempoPlElecPtr[i].boundaryField()[patchID_in]
			 +cClElecPtr[i].boundaryField()[patchID_in]+cNaElecPtr[i].boundaryField()[patchID_in]
			 )); 

	scalar flux_out = gSum(mag(UFluidPtr[i].boundaryField()[patchID_out])*fluidRegions[i].magSf().boundaryField()[patchID_out]*
                         (cTempoElecPtr[i].boundaryField()[patchID_out]+cTempoPlElecPtr[i].boundaryField()[patchID_out]
                         +cClElecPtr[i].boundaryField()[patchID_out]+cNaElecPtr[i].boundaryField()[patchID_out]
                         ));

	scalar flux_mem = (Itot/ApatchMem)/(-1*Foam::constant::physicoChemical::F.value());


        //scalar flux_etc = gSum(-kappaEPtr[i].boundaryField()[patchID_etc]*(phiElecPtr[i].boundaryField()[patchID_etc].snGrad() * fluidRegions[i].magSf().boundaryField()[patchID_etc]))
        //                        /Foam::constant::physicoChemical::F.value();
        
        scalar flux_sum = flux_in + flux_out + flux_mem;

        Info << "flux_inlet = " << flux_in << token::TAB << " flux_outlet = " << flux_out << token::TAB << " flux_mem = " << flux_mem << token::TAB << " Sum = " << flux_sum << nl << endl;

