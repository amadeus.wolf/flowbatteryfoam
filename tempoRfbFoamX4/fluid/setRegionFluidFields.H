    
// setting fluid mesh
    fvMesh& mesh = fluidRegions[i];


// setting fluid nonDim properties
//    scalar& phiCharElec = phiCharElecPtr[i];
//    scalar& lCharElec = lCharElecPtr[i];
//    scalar& cCharElec = cCharElecPtr[i];

// setting fluid transport properties
    volScalarField& DiffKoeffTempo = DiffKoeffTempoPtr[i];
    volScalarField& DiffKoeffTempoPl = DiffKoeffTempoPlPtr[i];
    volScalarField& DiffKoeffCl = DiffKoeffClPtr[i];
    volScalarField& DiffKoeffNa = DiffKoeffNaPtr[i];
    volScalarField& kappaE = kappaEPtr[i];
    volScalarField& TemperatureElec = TemperatureElecPtr[i];
//    volScalarField& kappaD = kappaDPtr[i];
//    volScalarField& HittorfNbr = HittorfNbrPtr[i];

    
// setting fluid fields
//    volScalarField& cLiElec = cLiElecPtr[i];
//    volVectorField& gradcLiElec = gradcLiElecPtr[i];

    volScalarField& cTempo = cTempoElecPtr[i];
    volVectorField& gradcTempo = gradcTempoElecPtr[i];
    volScalarField& cTempoPl = cTempoPlElecPtr[i];
    volVectorField& gradcTempoPl = gradcTempoPlElecPtr[i];
    volScalarField& cCl = cClElecPtr[i];
    volVectorField& gradcCl = gradcClElecPtr[i];
    volScalarField& cNa = cNaElecPtr[i];
    volVectorField& gradcNa = gradcNaElecPtr[i];

    volScalarField& cElecVolume = cElecVolumePtr[i];

    volScalarField& phiElec = phiElecPtr[i];
    volVectorField& gradPhiElec = gradPhiElecPtr[i];
    volVectorField& U = UFluidPtr[i];			// change AW
    surfaceScalarField& phi = phiPtr[i];		// change AW 28.4.

// physical constants
    #include "constants.H"
    const dimensionedScalar F = Foam::constant::physicoChemical::F;         // C/mol
    const dimensionedScalar R = Foam::constant::physicoChemical::R;


// obtaining coupling information from cathode_currentcollector

    label patchID_cte = solidRegions[i].boundaryMesh().findPatchID("cathode_to_electrolyte");
//    label patchID_cc = solidRegions[i].boundaryMesh().findPatchID("cathode_currentcollector");
   
    label patchID_in  = fluidRegions[i].boundaryMesh().findPatchID("inlet");
    label patchID_out = fluidRegions[i].boundaryMesh().findPatchID("outlet");
    label patchID_elec1  = fluidRegions[i].boundaryMesh().findPatchID("electrolyte_1");
    label patchID_elec2  = fluidRegions[i].boundaryMesh().findPatchID("electrolyte_2");
    label patchID_mem = fluidRegions[i].boundaryMesh().findPatchID("membrane");
    label patchID_etc = fluidRegions[i].boundaryMesh().findPatchID("electrolyte_to_cathode");

    scalar Itot = gSum(-conductSolidPtr[i].boundaryField()[patchID_cte]*(phiSolidPtr[i].boundaryField()[patchID_cte].snGrad() * solidRegions[i].magSf().boundaryField()[patchID_cte]) );
  
    Info << "Itot at cathode-to-electrolyte = " << Itot << " (saved in iSolidDict to use it in membrane BC)" << nl << endl;

// set fluid current convergence
    scalar totalCurrentToleranceFluid = 2e-3;	// 0.2%

// and you're done

// setting cLiElec at anode_currentcollector
/*label patchID_ac = fluidRegions[i].boundaryMesh().findPatchID("anode_currentcollector");
scalarField cLiElecInt = cLiElecPtr[i].boundaryField()[patchID_ac].patchInternalField();
vectorField gradcLiElecInt = gradcLiElecPtr[i].boundaryField()[patchID_ac].patchInternalField();

vectorField n = fluidRegions[i].Sf().boundaryField()[patchID_ac]/fluidRegions[i].magSf().boundaryField()[patchID_ac];
scalarField patchDeltas = fluidRegions[i].deltaCoeffs().boundaryField()[patchID_ac];
*/
scalar ApatchMem = gSum(fluidRegions[i].magSf().boundaryField()[patchID_mem]);

// save current in dictionary to use it in BC
iSolidDict.set("iSolid", Itot/ApatchMem);



