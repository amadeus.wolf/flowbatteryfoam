//PtrList<volScalarField> DiffKoeffLiElecPtr(fluidRegions.size());
PtrList<volScalarField> TemperatureElecPtr(fluidRegions.size());
PtrList<volScalarField> kappaEPtr(fluidRegions.size());
//PtrList<volScalarField> kappaDPtr(fluidRegions.size());
//PtrList<volScalarField> HittorfNbrPtr(fluidRegions.size());

// This is new
PtrList<volScalarField> DiffKoeffTempoPtr(fluidRegions.size());
PtrList<volScalarField> DiffKoeffTempoPlPtr(fluidRegions.size());
PtrList<volScalarField> DiffKoeffClPtr(fluidRegions.size());
PtrList<volScalarField> DiffKoeffNaPtr(fluidRegions.size());


    IOdictionary iSolidDict
    (
        IOobject
        (
            "iSolidDict",
            fluidRegions[0].time().constant(),
            fluidRegions[0],
            IOobject::NO_READ,
            IOobject::NO_WRITE
        )
    );

forAll(fluidRegions, i)
{

Info << "Reading transportProperties for fluidRegion " << fluidRegions[i].name() << nl << endl;

    fvMesh& mesh = fluidRegions[i];

    IOdictionary fluidTransportProperties
    (
        IOobject
        (
            "fluidTransportProperties",
            mesh.time().constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

// Define species diffusion coefficients
// Tempo

    IOobject DiffKoeffTempoIO
    (
        "DiffKoeffTempo",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );

    const dictionary& DiffKoeffTempoDict(fluidTransportProperties.subDict("DiffKoeffTempo"));
    word DiffKoeffTempoType(DiffKoeffTempoDict.lookup("type"));
    
    if (DiffKoeffTempoType == "uniform")
    {
        scalar DiffKoeffTempoValue(readScalar(DiffKoeffTempoDict.lookup("value")));

        DiffKoeffTempoPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffTempoIO,
                mesh,
                dimensionedScalar
                (
                    "DiffKoeffTempo",
                    dimensionSet(0, 2, -1 , 0, 0),
                    DiffKoeffTempoValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(DiffKoeffTempoType == "field")
    {
        DiffKoeffTempoIO.readOpt() = IOobject::MUST_READ;

        DiffKoeffTempoPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffTempoIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for DiffKoeffTempo"
            << abort(FatalError);
    }

// TempoPl
    IOobject DiffKoeffTempoPlIO
    (
        "DiffKoeffTempoPl",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );

    const dictionary& DiffKoeffTempoPlDict(fluidTransportProperties.subDict("DiffKoeffTempoPl"));
    word DiffKoeffTempoPlType(DiffKoeffTempoPlDict.lookup("type"));
    
    if (DiffKoeffTempoPlType == "uniform")
    {
        scalar DiffKoeffTempoPlValue(readScalar(DiffKoeffTempoPlDict.lookup("value")));

        DiffKoeffTempoPlPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffTempoPlIO,
                mesh,
                dimensionedScalar
                (
                    "DiffKoeffTempoPl",
                    dimensionSet(0, 2, -1 , 0, 0),
                    DiffKoeffTempoPlValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(DiffKoeffTempoPlType == "field")
    {
        DiffKoeffTempoPlIO.readOpt() = IOobject::MUST_READ;

        DiffKoeffTempoPlPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffTempoPlIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for DiffKoeffTempoPl"
            << abort(FatalError);
    }

// Sodium Ion Na+ (Na)
    IOobject DiffKoeffNaIO
    (
        "DiffKoeffNa",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );

    const dictionary& DiffKoeffNaDict(fluidTransportProperties.subDict("DiffKoeffNa"));
    word DiffKoeffNaType(DiffKoeffNaDict.lookup("type"));
    
    if (DiffKoeffNaType == "uniform")
    {
        scalar DiffKoeffNaValue(readScalar(DiffKoeffNaDict.lookup("value")));

        DiffKoeffNaPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffNaIO,
                mesh,
                dimensionedScalar
                (
                    "DiffKoeffNa",
                    dimensionSet(0, 2, -1 , 0, 0),
                    DiffKoeffNaValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(DiffKoeffNaType == "field")
    {
        DiffKoeffNaIO.readOpt() = IOobject::MUST_READ;

        DiffKoeffNaPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffNaIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for DiffKoeffNa"
            << abort(FatalError);
    }
// Cloride ion Cl- (Cl)
    IOobject DiffKoeffClIO
    (
        "DiffKoeffCl",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );

    const dictionary& DiffKoeffClDict(fluidTransportProperties.subDict("DiffKoeffCl"));
    word DiffKoeffClType(DiffKoeffClDict.lookup("type"));
    
    if (DiffKoeffClType == "uniform")
    {
        scalar DiffKoeffClValue(readScalar(DiffKoeffClDict.lookup("value")));

        DiffKoeffClPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffClIO,
                mesh,
                dimensionedScalar
                (
                    "DiffKoeffCl",
                    dimensionSet(0, 2, -1 , 0, 0),
                    DiffKoeffClValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(DiffKoeffClType == "field")
    {
        DiffKoeffClIO.readOpt() = IOobject::MUST_READ;

        DiffKoeffClPtr.set
        (
	    i,
            new volScalarField
            (
                DiffKoeffClIO,
                mesh
            )
        );
    }   
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for DiffKoeffCl"
            << abort(FatalError);
    }

// Temperature electrolyte    
    IOobject TemperatureElecIO
    (
        "TemperatureElec",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );


    const dictionary& TemperatureElecDict(fluidTransportProperties.subDict("TemperatureElec"));
    word TemperatureElecType(TemperatureElecDict.lookup("type"));
    if (TemperatureElecType == "uniform")
    {
        scalar TemperatureElecValue(readScalar(TemperatureElecDict.lookup("value")));

        TemperatureElecPtr.set
        (
	    i,
            new volScalarField
            (
                TemperatureElecIO,
                mesh,
                dimensionedScalar
                (
                    "TemperatureElec",
                    dimensionSet(0, 0, 0, 1, 0),
                    TemperatureElecValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(TemperatureElecType == "field")
    {
        TemperatureElecIO.readOpt() = IOobject::MUST_READ;

        TemperatureElecPtr.set
        (
	    i,
            new volScalarField
            (
                TemperatureElecIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for TemperatureElec"
            << abort(FatalError);
    }

// KappaE: effective electrical conductivity electrolyte in [S/m]
     IOobject kappaEIO
    (
        "kappaE",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );


    const dictionary& kappaEDict(fluidTransportProperties.subDict("kappaE"));
    word kappaEType(kappaEDict.lookup("type"));
    if (kappaEType == "uniform")
    {
        scalar kappaEValue(readScalar(kappaEDict.lookup("value")));

        kappaEPtr.set
        (
	    i,
            new volScalarField
            (
                kappaEIO,
                mesh,
                dimensionedScalar
                (
                    "kappaE",
                    dimensionSet(-1, -3, 3, 0, 0, 2, 0),
                    kappaEValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(kappaEType == "field")
    {
        kappaEIO.readOpt() = IOobject::MUST_READ;

        kappaEPtr.set
        (
	    i,
            new volScalarField
            (
                kappaEIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for kappaE"
            << abort(FatalError);
    }



/*    IOobject HittorfNbrIO
    (
        "HittorfNbr",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );


    const dictionary& HittorfNbrDict(fluidTransportProperties.subDict("HittorfNbr"));
    word HittorfNbrType(HittorfNbrDict.lookup("type"));
    if (HittorfNbrType == "uniform")
    {
        scalar HittorfNbrValue(readScalar(HittorfNbrDict.lookup("value")));

        HittorfNbrPtr.set
        (
	    i,
            new volScalarField
            (
                HittorfNbrIO,
                mesh,
                dimensionedScalar
                (
                    "HittorfNbr",
                    dimensionSet(0, 0, 0, 0, 0),
                    HittorfNbrValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(HittorfNbrType == "field")
    {
        HittorfNbrIO.readOpt() = IOobject::MUST_READ;

        HittorfNbrPtr.set
        (
	    i,
            new volScalarField
            (
                HittorfNbrIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for HittorfNbr"
            << abort(FatalError);
    }
*/


/*    IOobject kappaDIO
    (
        "kappaD",
        runTime.timeName(0),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    );


    const dictionary& kappaDDict(fluidTransportProperties.subDict("kappaD"));
    word kappaDType(kappaDDict.lookup("type"));
    if (kappaDType == "uniform")
    {
        scalar kappaEValue = readScalar(fluidTransportProperties.subDict("kappaE").lookup("value"));
	scalar TemperatureElecValue = readScalar(fluidTransportProperties.subDict("TemperatureElec").lookup("value"));
	scalar HittorfNbrValue = readScalar(fluidTransportProperties.subDict("HittorfNbr").lookup("value"));

	scalar kappaDValue=kappaEValue*HittorfNbrValue*Foam::constant::physicoChemical::R.value()*TemperatureElecValue/(Foam::constant::physicoChemical::F.value());

        kappaDPtr.set
        (
	    i,
            new volScalarField
            (
                kappaDIO,
                mesh,
                dimensionedScalar
                (
                    "kappaD",
                    dimensionSet(0, -1, 0, 0, 0, 1, 0),
                    kappaDValue
                ),
                zeroGradientFvPatchField<scalar>::typeName
            )
        );

    }
    else if(kappaDType == "field")
    {
        kappaDIO.readOpt() = IOobject::MUST_READ;

        kappaDPtr.set
        (
	    i,
            new volScalarField
            (
                kappaDIO,
                mesh
            )
        );
    }
    else
    {
        FatalErrorIn
        (
             "readFluidTransportProperties.H"
        )   << "Valid type entries are uniform or field for kappaD"
            << abort(FatalError);
    }

*/
}
