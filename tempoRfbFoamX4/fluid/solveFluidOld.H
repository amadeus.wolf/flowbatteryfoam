
Info << "Itot at currentcollector is " << Itot << endl;
        int iCorr = 0;

        do
        {

	Info << "arbitrary value of U[33] = " << U[33] << "  and cLiElec[33] = " << cLiElec[33] << endl;

	// solution of the concentration-field

	fvScalarMatrix cLiElecEqn
	(
		fvm::ddt(cLiElec) //+ (U & fvc::grad(cLiElec))
	      ==
		fvm::laplacian(DiffKoeffLiElec,cLiElec) - (U & fvc::grad(cLiElec))	// last term is convection
		// 1/sqr(lCharElec)*fvm::laplacian(DiffKoeffLiElec,cLiElec)
	);

	
	cLiElecEqn.solve();

	gradcLiElec = fvc::grad(cLiElec);	
	gradcLiElecInt = gradcLiElecPtr[i].boundaryField()[patchID_ac].patchInternalField();

	Info << "arbitrary value of U[33] = " << U[33] << "  and cLiElec[33] = " << cLiElec[33] << endl;

	// solution of potential-field
	
	fvScalarMatrix phiElecEqn 
	(
		fvm::laplacian(kappaE*phiCharElec,phiElec)
	      + fvc::laplacian(kappaD/cLiElec,cLiElec)
	);

	phiElecEqn.solve();

	phiResidual = gSum(-DiffKoeffLiElec.boundaryField()[patchID_r11]*cCharElec*lCharElec*(gradcLiElec.boundaryField()[patchID_r11] & fluidRegions[i].Sf().boundaryField()[patchID_r11])) + gSum(-DiffKoeffLiElec.boundaryField()[patchID_r12]*cCharElec*lCharElec*(gradcLiElec.boundaryField()[patchID_r12] & fluidRegions[i].Sf().boundaryField()[patchID_r12])) + gSum(-DiffKoeffLiElec.boundaryField()[patchID_r21]*cCharElec*lCharElec*(gradcLiElec.boundaryField()[patchID_r21] & fluidRegions[i].Sf().boundaryField()[patchID_r21])) + gSum(-DiffKoeffLiElec.boundaryField()[patchID_r22]*cCharElec*lCharElec*(gradcLiElec.boundaryField()[patchID_r22] & fluidRegions[i].Sf().boundaryField()[patchID_r22]));

Info << nl << nl << "aklsdjfalsdjkfjkaskjafjad is " << phiResidual << nl << nl << endl;




//	phiElec.relax();

	gradPhiElec = fvc::grad(phiElec);


//	cLiElecEqn.clear();
//	phiElecEqn.clear();

        } while (phiResidual > convergenceTolerance && ++iCorr < nCorr);



